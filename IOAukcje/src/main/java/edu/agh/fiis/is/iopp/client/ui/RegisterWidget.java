package edu.agh.fiis.is.iopp.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalSplitPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.CaptionPanel;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.ListBox;

import edu.agh.fiis.is.iopp.client.RegisterService;
import edu.agh.fiis.is.iopp.client.RegisterServiceAsync;
import edu.agh.fiis.is.iopp.client.SecurityService;
import edu.agh.fiis.is.iopp.client.SecurityServiceAsync;
import edu.agh.fiis.is.iopp.shared.LoginResult;
import edu.agh.fiis.is.iopp.shared.RegisterResult;

public class RegisterWidget extends Composite {
	
	RegisterServiceAsync service = GWT.create(RegisterService.class);
	
	final TextBox textBox = new TextBox();
	final TextBox textBox_1 = new TextBox();
	final TextBox textBox_2 = new TextBox();
	final TextBox textBox_3 = new TextBox();
	final TextBox textBox_4 = new TextBox();
	final TextBox textBox_5 = new TextBox();
	final TextBox textBox_6 = new TextBox();
	final TextBox textBox_8 = new TextBox();
	final ListBox comboBox = new ListBox();
	
	public RegisterWidget() {
		RootPanel.get("content").clear();
		FormPanel formPanel = new FormPanel();
		initWidget(formPanel);
		formPanel.setHeight("562px");
		
		VerticalPanel verticalPanel = new VerticalPanel();
		formPanel.setWidget(verticalPanel);
		verticalPanel.setSize("100%", "563px");
		
		CaptionPanel captionPanel = new CaptionPanel("Account Details");
		captionPanel.setStyleName("register");
		verticalPanel.add(captionPanel);
		captionPanel.setHeight("82px");
		
		HorizontalSplitPanel horizontalSplitPanel = new HorizontalSplitPanel();
		captionPanel.setContentWidget(horizontalSplitPanel);
		horizontalSplitPanel.setSize("419px", "3cm");
		
		VerticalPanel verticalPanel_1 = new VerticalPanel();
		horizontalSplitPanel.setLeftWidget(verticalPanel_1);
		verticalPanel_1.setSize("100%", "72px");
		
		Label label = new Label("E-mail:");
		verticalPanel_1.add(label);
		
		Label label_1 = new Label("Password:");
		verticalPanel_1.add(label_1);
		
		VerticalPanel verticalPanel_2 = new VerticalPanel();
		horizontalSplitPanel.setRightWidget(verticalPanel_2);
		verticalPanel_2.setSize("100%", "72px");
		
		verticalPanel_2.add(textBox);
		verticalPanel_2.add(textBox_1);
		
		CaptionPanel captionPanel_1 = new CaptionPanel("Personal Details");
		captionPanel_1.setStyleName("register");
		verticalPanel.add(captionPanel_1);
		captionPanel_1.setHeight("199px");
		
		HorizontalSplitPanel horizontalSplitPanel_1 = new HorizontalSplitPanel();
		captionPanel_1.setContentWidget(horizontalSplitPanel_1);
		horizontalSplitPanel_1.setSize("417px", "187px");
		
		VerticalPanel verticalPanel_3 = new VerticalPanel();
		horizontalSplitPanel_1.setLeftWidget(verticalPanel_3);
		verticalPanel_3.setSize("100%", "181px");
		
		Label lblLogin = new Label("Login:");
		verticalPanel_3.add(lblLogin);
		
		Label lblCompanyName = new Label("Company Name:");
		verticalPanel_3.add(lblCompanyName);
		
		Label lblContactPhoneNumber = new Label("Contact phone number:");
		verticalPanel_3.add(lblContactPhoneNumber);
		
		Label lblAddressLine = new Label("Address line 1:");
		verticalPanel_3.add(lblAddressLine);
		
		Label lblAddressLine_1 = new Label("Address line 2:");
		verticalPanel_3.add(lblAddressLine_1);
		
		Label lblRegistrationCountry = new Label("Registration Country:");
		verticalPanel_3.add(lblRegistrationCountry);
		
		Label lblWebsite = new Label("Website:");
		verticalPanel_3.add(lblWebsite);
		
		VerticalPanel verticalPanel_4 = new VerticalPanel();
		horizontalSplitPanel_1.setRightWidget(verticalPanel_4);
		verticalPanel_4.setSize("100%", "176px");
		
		verticalPanel_4.add(textBox_2);
		
		verticalPanel_4.add(textBox_3);
		
		verticalPanel_4.add(textBox_4);
		
		verticalPanel_4.add(textBox_5);
		
		verticalPanel_4.add(textBox_6);
		
		comboBox.addItem("Afghanistan");
		comboBox.addItem("Albania");
		comboBox.addItem("Algeria");
		comboBox.addItem("Andorra");
		comboBox.addItem("Angola");
		comboBox.addItem("Antigua & Deps");
		comboBox.addItem("Argentina");
		comboBox.addItem("Armenia");
		comboBox.addItem("Australia");
		comboBox.addItem("Austria");
		comboBox.addItem("Azerbaijan");
		comboBox.addItem("Bahamas");
		comboBox.addItem("Bahrain");
		comboBox.addItem("Bangladesh");
		comboBox.addItem("Barbados");
		comboBox.addItem("Belarus");
		comboBox.addItem("Belgium");
		comboBox.addItem("Belize");
		comboBox.addItem("Benin");
		comboBox.addItem("Bhutan");
		comboBox.addItem("Bolivia");
		comboBox.addItem("Bosnia Herzegovina");
		comboBox.addItem("Botswana");
		comboBox.addItem("Brazil");
		comboBox.addItem("Brunei");
		comboBox.addItem("Bulgaria");
		comboBox.addItem("Burkina");
		comboBox.addItem("Burundi");
		comboBox.addItem("Cambodia");
		comboBox.addItem("Cameroon");
		comboBox.addItem("Canada");
		comboBox.addItem("Cape Verde");
		comboBox.addItem("Central African Rep");
		comboBox.addItem("Chad");
		comboBox.addItem("Chile");
		comboBox.addItem("China");
		comboBox.addItem("Colombia");
		comboBox.addItem("Comoros");
		comboBox.addItem("Congo");
		comboBox.addItem("Congo {Democratic Rep}");
		comboBox.addItem("Costa Rica");
		comboBox.addItem("Croatia");
		comboBox.addItem("Cuba");
		comboBox.addItem("Cyprus");
		comboBox.addItem("Czech Republic");
		comboBox.addItem("Denmark");
		comboBox.addItem("Djibouti");
		comboBox.addItem("Dominica");
		comboBox.addItem("Dominican Republic");
		comboBox.addItem("East Timor");
		comboBox.addItem("Ecuador");
		comboBox.addItem("Egypt");
		comboBox.addItem("El Salvador");
		comboBox.addItem("Equatorial Guinea");
		comboBox.addItem("Eritrea");
		comboBox.addItem("Estonia");
		comboBox.addItem("Ethiopia");
		comboBox.addItem("Fiji");
		comboBox.addItem("Finland");
		comboBox.addItem("France");
		comboBox.addItem("Gabon");
		comboBox.addItem("Gambia");
		comboBox.addItem("Georgia");
		comboBox.addItem("Germany");
		comboBox.addItem("Ghana");
		comboBox.addItem("Greece");
		comboBox.addItem("Grenada");
		comboBox.addItem("Guatemala");
		comboBox.addItem("Guinea");
		comboBox.addItem("Guinea-Bissau");
		comboBox.addItem("Guyana");
		comboBox.addItem("Haiti");
		comboBox.addItem("Honduras");
		comboBox.addItem("Hungary");
		comboBox.addItem("Iceland");
		comboBox.addItem("India");
		comboBox.addItem("Indonesia");
		comboBox.addItem("Iran");
		comboBox.addItem("Iraq");
		comboBox.addItem("Ireland {Republic}");
		comboBox.addItem("Israel");
		comboBox.addItem("Italy");
		comboBox.addItem("Ivory Coast");
		comboBox.addItem("Jamaica");
		comboBox.addItem("Japan");
		comboBox.addItem("Jordan");
		comboBox.addItem("Kazakhstan");
		comboBox.addItem("Kenya");
		comboBox.addItem("Kiribati");
		comboBox.addItem("Korea North");
		comboBox.addItem("Korea South");
		comboBox.addItem("Kosovo");
		comboBox.addItem("Kuwait");
		comboBox.addItem("Kyrgyzstan");
		comboBox.addItem("Laos");
		comboBox.addItem("Latvia");
		comboBox.addItem("Lebanon");
		comboBox.addItem("Lesotho");
		comboBox.addItem("Liberia");
		comboBox.addItem("Libya");
		comboBox.addItem("Liechtenstein");
		comboBox.addItem("Lithuania");
		comboBox.addItem("Luxembourg");
		comboBox.addItem("Macedonia");
		comboBox.addItem("Madagascar");
		comboBox.addItem("Malawi");
		comboBox.addItem("Malaysia");
		comboBox.addItem("Maldives");
		comboBox.addItem("Mali");
		comboBox.addItem("Malta");
		comboBox.addItem("Marshall Islands");
		comboBox.addItem("Mauritania");
		comboBox.addItem("Mauritius");
		comboBox.addItem("Mexico");
		comboBox.addItem("Micronesia");
		comboBox.addItem("Moldova");
		comboBox.addItem("Monaco");
		comboBox.addItem("Mongolia");
		comboBox.addItem("Montenegro");
		comboBox.addItem("Morocco");
		comboBox.addItem("Mozambique");
		comboBox.addItem("Myanmar, {Burma}");
		comboBox.addItem("Namibia");
		comboBox.addItem("Nauru");
		comboBox.addItem("Nepal");
		comboBox.addItem("Netherlands");
		comboBox.addItem("New Zealand");
		comboBox.addItem("Nicaragua");
		comboBox.addItem("Niger");
		comboBox.addItem("Nigeria");
		comboBox.addItem("Norway");
		comboBox.addItem("Oman");
		comboBox.addItem("Pakistan");
		comboBox.addItem("Palau");
		comboBox.addItem("Panama");
		comboBox.addItem("Papua New Guinea");
		comboBox.addItem("Paraguay");
		comboBox.addItem("Peru");
		comboBox.addItem("Philippines");
		comboBox.addItem("Poland");
		comboBox.addItem("Portugal");
		comboBox.addItem("Qatar");
		comboBox.addItem("Romania");
		comboBox.addItem("Russian Federation");
		comboBox.addItem("Rwanda");
		comboBox.addItem("St Kitts & Nevis");
		comboBox.addItem("St Lucia");
		comboBox.addItem("Saint Vincent & the Grenadines");
		comboBox.addItem("Samoa");
		comboBox.addItem("San Marino");
		comboBox.addItem("Sao Tome & Principe");
		comboBox.addItem("Saudi Arabia");
		comboBox.addItem("Senegal");
		comboBox.addItem("Serbia");
		comboBox.addItem("Seychelles");
		comboBox.addItem("Sierra Leone");
		comboBox.addItem("Singapore");
		comboBox.addItem("Slovakia");
		comboBox.addItem("Slovenia");
		comboBox.addItem("Solomon Islands");
		comboBox.addItem("Somalia");
		comboBox.addItem("South Africa");
		comboBox.addItem("South Sudan");
		comboBox.addItem("Spain");
		comboBox.addItem("Sri Lanka");
		comboBox.addItem("Sudan");
		comboBox.addItem("Suriname");
		comboBox.addItem("Swaziland");
		comboBox.addItem("Sweden");
		comboBox.addItem("Switzerland");
		comboBox.addItem("Syria");
		comboBox.addItem("Taiwan");
		comboBox.addItem("Tajikistan");
		comboBox.addItem("Tanzania");
		comboBox.addItem("Thailand");
		comboBox.addItem("Togo");
		comboBox.addItem("Tonga");
		comboBox.addItem("Trinidad & Tobago");
		comboBox.addItem("Tunisia");
		comboBox.addItem("Turkey");
		comboBox.addItem("Turkmenistan");
		comboBox.addItem("Tuvalu");
		comboBox.addItem("Uganda");
		comboBox.addItem("Ukraine");
		comboBox.addItem("United Arab Emirates");
		comboBox.addItem("United Kingdom");
		comboBox.addItem("United States");
		comboBox.addItem("Uruguay");
		comboBox.addItem("Uzbekistan");
		comboBox.addItem("Vanuatu");
		comboBox.addItem("Vatican City");
		comboBox.addItem("Venezuela");
		comboBox.addItem("Vietnam");
		comboBox.addItem("Yemen");
		comboBox.addItem("Zambia");
		comboBox.addItem("Zimbabwe");
		verticalPanel_4.add(comboBox);
		
		verticalPanel_4.add(textBox_8);
		
		CaptionPanel captionPanel_2 = new CaptionPanel("Informations");
		verticalPanel.add(captionPanel_2);
		
		CaptionPanel captionPanel_3 = new CaptionPanel("Terms and conditions");
		verticalPanel.add(captionPanel_3);
		
		CheckBox checkBox = new CheckBox("I accept the terms and conditions");
		captionPanel_3.setContentWidget(checkBox);
		checkBox.setSize("5cm", "3cm");
		
		CaptionPanel captionPanel_4 = new CaptionPanel("Actions");
		verticalPanel.add(captionPanel_4);
		
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		captionPanel_4.setContentWidget(horizontalPanel);
		horizontalPanel.setSize("419px", "44px");
		
		Button button = new Button("Register");
		button.setStyleName("button");
		button.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				callRegisterService();
				// TODO Walidacje danych na formatce
				
				//TODO Ustalic sposob zapisu
			}
		});
		horizontalPanel.add(button);
		
		Button button_1 = new Button("Clear");
		button_1.setStyleName("button");
		horizontalPanel.add(button_1);
		
	}
	
	private void callRegisterService() {

		AsyncCallback<RegisterResult> callback = new AsyncCallback<RegisterResult>() {

			@Override
			public void onFailure(Throwable caught) {
				setErrorMessage("Wystapil blad: " + caught.getMessage());
			}

			private void setErrorMessage(String string) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(RegisterResult result) {
				if (result.isSuccess()) {
					RootPanel.get("content").clear();
					FlowPanel layout = new FlowPanel();
					layout.add(new Label("Witaj w zasobie chronionym!"));

					Button logoutBtn = new Button("OK");
					layout.add(logoutBtn);

					RootPanel.get("content").add(layout);
				} else {
					setErrorMessage(result.getErrorMessage());
				}
			}
		};
		service.registerCompanyParty(textBox.getText(),textBox_1.getText(),textBox_2.getText(),textBox_3.getText(),textBox_4.getText(), textBox_5.getText(), textBox_6.getText(), comboBox.getItemText(comboBox.getSelectedIndex()),textBox_8.getText(),callback);
	}
	/*
	private static LoginWidgetUiBinder uiBinder = GWT
			.create(LoginWidgetUiBinder.class);
	final Logger logger = Logger.getLogger("Login Page");

	SecurityServiceAsync service = GWT.create(SecurityService.class);

	interface LoginWidgetUiBinder extends UiBinder<Widget, LoginWidget> {
	}*/

}
