package edu.agh.fiis.is.iopp.client;

import com.google.gwt.i18n.client.Messages;

public interface IOAukcjeMessages extends Messages{

	@DefaultMessage("''{0}'' is not a valid login format.")
	String invalidlogin(String login);

	@DefaultMessage("Login or password incorrect. Please try again.")
	  String logInFailure();
	  
	@DefaultMessage("Thank you ''{0}''. Logging you in...")
	  String logInSuccess(String login);

}
