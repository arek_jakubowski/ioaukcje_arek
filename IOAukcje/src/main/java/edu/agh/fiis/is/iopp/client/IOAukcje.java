package edu.agh.fiis.is.iopp.client;

//https://ldzieran@bitbucket.org/ioteam/ioaukcje.git

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.agh.fiis.is.iopp.client.ui.LoginWidget;
import edu.agh.fiis.is.iopp.client.ui.MainMenuWidget;
import edu.agh.fiis.is.iopp.client.ui.RegisterWidget;

public class IOAukcje implements EntryPoint {

	final Logger logger = Logger.getLogger("IOAUKCJE");
	SecurityServiceAsync security = GWT.create(SecurityService.class);
	private final IOAukcjeConstants constants = GWT
			.create(IOAukcjeConstants.class);
	private final IOAukcjeMessages messages = GWT
			.create(IOAukcjeMessages.class);
	private VerticalPanel mainPanel = new VerticalPanel();
	private HorizontalPanel menuPanel = new HorizontalPanel();
	final Label menuHomepage = new Label();
	final Label menuLogIn = new Label();
	final LoginWidget loginBox = new LoginWidget();
	final RegisterWidget registerBox = new RegisterWidget();
	final MainMenuWidget menu = new MainMenuWidget();
	final MenuBar menuBar = new MenuBar();
	final Label errorLabel = new Label();
	final Label welcomeMessage = new Label();
	final Label loginIstructions = new Label();

	public void onModuleLoad() {

		GWT.setUncaughtExceptionHandler(new GWT.UncaughtExceptionHandler() {

			public void onUncaughtException(Throwable caught) {
				logger.log(Level.SEVERE, "Woops! Uncaught Exception");
				displayError("Woops! Uncaught Exception");
			}
		});

		Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
			public void execute() {
				init();
			}
		});
	}

	private void init() {

		Window.setTitle(constants.title());
		welcomeMessage.setText(constants.welcomeMessage());
		loginIstructions.setText(constants.loginInstructions());
		menuBar.addStyleName("menu");
		menuBar.addItem("Homepage", new Command() {
			@Override
			public void execute() {
				Window.Location.replace(GWT.getHostPageBaseURL());
			}
		});
		menuBar.addItem("Log in", new Command() {
			@Override
			public void execute() {
				RootPanel.get("content").add(loginBox);
			}
		});
		menuBar.addItem("Register", new Command() {
			@Override
			public void execute() {
				RootPanel.get("content").add(registerBox);
			}
		});
		
		// TODO constants
		//menuHomepage.setText("Homepage");
		//menuLogIn.setText("Log In");
		//menuHomepage.setStyleName("myMenu");

		RootPanel.get("menu").add(menuBar);
		menuPanel.add(menu);
		mainPanel.add(welcomeMessage);
		mainPanel.add(loginIstructions);
		mainPanel.add(errorLabel);

		//RootPanel.get("content").add(loginBox);

	}

	private void callLogoutService() {
		AsyncCallback<Void> callback = new AsyncCallback<Void>() {

			public void onFailure(Throwable caught) {
				logger.log(Level.SEVERE,
						"Woops! Failiure calling Logout Service.");
				displayError("Woops! Failiure calling Logout Service.");
			}

			public void onSuccess(Void result) {
				Window.Location.reload();
			}
		};

		security.logout(callback);
	}

	private void displayError(String error) {
		errorLabel.setText(error);
		errorLabel.setVisible(true);
	}
}
