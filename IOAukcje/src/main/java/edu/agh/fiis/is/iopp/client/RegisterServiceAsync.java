package edu.agh.fiis.is.iopp.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.agh.fiis.is.iopp.shared.LoginResult;
import edu.agh.fiis.is.iopp.shared.RegisterResult;

public interface RegisterServiceAsync {
  
  void registerCompanyParty(String emial, String password, String login, String cp_name, String cp_phone, String cp_address_1, String cp_address_2, String counrty, String website ,AsyncCallback<RegisterResult> callback);

}
