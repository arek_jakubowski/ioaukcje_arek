package edu.agh.fiis.is.iopp.server;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.util.SavedRequest;
import org.apache.shiro.web.util.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import edu.agh.fiis.is.iopp.client.SecurityService;
import edu.agh.fiis.is.iopp.shared.LoginResult;

public class SecurityServiceImpl extends RemoteServiceServlet implements
		SecurityService {
	private static final long serialVersionUID = -4935507026700430314L;

	private Logger LOG = LoggerFactory.getLogger(SecurityServiceImpl.class);

	@Override
	public LoginResult login(String username, String password,
			boolean rememberMe) {
		LOG.info("LOGIN (username: " + username + " password: " + password
				+ ")");
		Subject subject = SecurityUtils.getSubject();
		LoginResult result = new LoginResult();
		result.setSuccess(false);
		result.setReferrerUrl(getReferrerUrl());

		if (subject.isAuthenticated()) {
			LOG.info("ZALOGOWANO !");
			result.setSuccess(true);
			return result;
		}

		UsernamePasswordToken token = new UsernamePasswordToken(username,
				password);
		token.setRememberMe(rememberMe);
		doLogin(subject, token, result);

		return result;
	}

	@Override
	public void logout() {
		LOG.info("LOGOUT");
		Subject currentUser = SecurityUtils.getSubject();
		currentUser.logout();
	}

	protected void doLogin(Subject subject, AuthenticationToken token,
			LoginResult result) {
		try {
			subject.login(token);
			result.setSuccess(true);
		} catch (UnknownAccountException uae) {
			// username wasn't in the system, show them an error message?
			result.setErrorMessage("Konto nie istnieje");
		} catch (IncorrectCredentialsException ice) {
			// password didn't match, try again?
			result.setErrorMessage("Wprowadzona nazwa uzytkownika lub haslo jest nieprawidlowe.");
		} catch (LockedAccountException lae) {
			// account for that username is locked - can't login. Show them a
			// message?
			result.setErrorMessage("Konto jest zablokowane");
		} catch (AuthenticationException ae) {
			// unexpected condition - error?
			result.setErrorMessage("Wystapil nieznany blad");
		}
	}

	protected String getReferrerUrl() {
		SavedRequest sr = WebUtils.getSavedRequest(getThreadLocalRequest());
		if (sr != null) {
			LOG.info("Request Url: " + sr.getRequestUrl());
			return sr.getRequestUrl();
		}

		return null;
	}
}