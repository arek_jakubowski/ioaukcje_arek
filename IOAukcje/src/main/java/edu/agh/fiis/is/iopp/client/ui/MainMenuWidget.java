package edu.agh.fiis.is.iopp.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.uibinder.client.UiBinder;

public class MainMenuWidget extends Composite {
	
	private static MainMenuUiBinder uiBinder = GWT
			.create(MainMenuUiBinder.class);
	
	interface MainMenuUiBinder extends UiBinder<Widget, MainMenuWidget> {
	}
	
	@UiField
	Label homepage;
	@UiField
	Label page1;
	@UiField
	Label page2;
	@UiField
	Label page3;
	@UiField
	Label logIn;
	@UiField
	Label logOut;
	
	public MainMenuWidget() {
		initWidget(uiBinder.createAndBindUi(this));
	}

}
