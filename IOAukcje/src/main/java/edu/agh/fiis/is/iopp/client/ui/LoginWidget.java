package edu.agh.fiis.is.iopp.client.ui;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import edu.agh.fiis.is.iopp.client.SecurityService;
import edu.agh.fiis.is.iopp.client.SecurityServiceAsync;
import edu.agh.fiis.is.iopp.shared.LoginResult;

public class LoginWidget extends Composite {

	private static LoginWidgetUiBinder uiBinder = GWT
			.create(LoginWidgetUiBinder.class);
	final Logger logger = Logger.getLogger("Login Page");

	SecurityServiceAsync service = GWT.create(SecurityService.class);

	interface LoginWidgetUiBinder extends UiBinder<Widget, LoginWidget> {
	}

	@UiField
	TextBox username;
	@UiField
	PasswordTextBox password;
	@UiField
	CheckBox rememberMe;
	@UiField
	Button submit;
	@UiField
	Label info;

	public LoginWidget() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@UiHandler("submit")
	void onFormSubmit(ClickEvent e) {
		if (!isFormValid()) {
			e.preventDefault();
			setErrorMessage("Musisz podac login i haslo!");
		} else {
			callLoginService();
		}
	}

	private void setErrorMessage(String message) {
		info.setText(message);
	}

	private boolean isFormValid() {
		return username.getValue().length() > 0
				&& password.getValue().length() > 0;
	}

	private void callLoginService() {

		AsyncCallback<LoginResult> callback = new AsyncCallback<LoginResult>() {

			@Override
			public void onFailure(Throwable caught) {
				setErrorMessage("Wystapil blad: " + caught.getMessage());
			}

			@Override
			public void onSuccess(LoginResult result) {
				if (result.isSuccess()) {
					RootPanel.get("content").clear();
					FlowPanel layout = new FlowPanel();
					layout.add(new Label("Witaj w zasobie chronionym!"));

					Button logoutBtn = new Button("Wyloguj");
					logoutBtn.addClickHandler(new ClickHandler() {

						public void onClick(ClickEvent event) {
							callLogoutService();
						}
					});
					layout.add(logoutBtn);

					RootPanel.get("content").add(layout);
				} else {
					setErrorMessage(result.getErrorMessage());
				}
			}
		};

		service.login(username.getValue(), password.getValue(),
				rememberMe.getValue(), callback);
	}

	private void redirectOnSuccess() {
		Window.Location.replace(GWT.getHostPageBaseURL());
	}

	private void callLogoutService() {
		AsyncCallback<Void> callback = new AsyncCallback<Void>() {
			public void onFailure(Throwable caught) {
				logger.log(Level.SEVERE,
						"Woops! Failiure calling Logout Service.");
			}

			public void onSuccess(Void result) {
				Window.Location.reload();
			}
		};
		service.logout(callback);
	}
}
