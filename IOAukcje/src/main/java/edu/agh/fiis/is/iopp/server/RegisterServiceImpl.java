package edu.agh.fiis.is.iopp.server;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import edu.agh.fiis.is.iopp.client.RegisterService;
import edu.agh.fiis.is.iopp.helpers.SessionFactoryHelper;
import edu.agh.fiis.is.iopp.shared.RegisterResult;
import edu.agh.fiis.is.iopp.um.CompanyParty;

public class RegisterServiceImpl extends RemoteServiceServlet implements
RegisterService {

	private static final long serialVersionUID = -5130025784799444971L;
	RegisterResult result = new RegisterResult();

	@Override
	public RegisterResult registerCompanyParty(String emial, String password,
			String login, String cp_name, String cp_phone, String cp_address_1,
			String cp_address_2, String counrty, String website) {
		Session sess = SessionFactoryHelper.getSessionFactory().openSession();
		Transaction tx = null;
		CompanyParty cp = null;
		try {
		    tx = sess.beginTransaction();
		    	//TODO Zapisac CompanyParty
		    	cp = new CompanyParty();
		    	cp.setEmail(emial);
		    	cp.setPassword(password);
		    	cp.setLogin(login);
		    	cp.setCompany_name(cp_name);
		    	cp.setPhone_number(cp_phone);
		    	cp.setAddress_line_1(cp_address_1);
		    	cp.setAddress_line_2(cp_address_2);
		    	cp.setCountry(counrty);
		    	cp.setWebsite(website);
		    	sess.persist(cp);
		        tx.commit();
		        result.setSuccess(true);
		        return result;
		}

		catch (RuntimeException e) {
		    if (tx != null) tx.rollback();
		    throw e; // or display error message
		}

		finally {
		    sess.close();
		}
		
	}


}
